from ch_be.settings.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS += ('storages',)
AWS_STORAGE_BUCKET_NAME = "ch-be"
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = S3_URL

DATABASES = {    
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'chbedb',
        'USER' : 'chbeusr',
        'PASSWORD' : 'chbeusrpass',
        'HOST' : 'chbedb.ca4t5ki7ms7v.us-west-2.rds.amazonaws.com',
        'PORT' : '5432',
    }
}
